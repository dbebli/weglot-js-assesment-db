FROM node:14

RUN npm install -g eslint mocha

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

CMD [ "node", "index.js" ]