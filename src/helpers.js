import fs from 'fs';

const getOutputFilesNumber = () => {
	const files = fs.readdirSync('./data');
	const outputFiles = files.filter(file => file.startsWith('output'));
	return outputFiles.length;
};

const inputsToArray = (inputList) => {
	const path = `./data/input${inputList}.txt`;
	if (!fs.existsSync(path)) {
		throw new Error('this file doesn\'t exist');
	}

	const text = fs.readFileSync(path).toString('utf-8');
	return text.split('\n');
};

const outputsToArray = () => {
	const outputsArray = [];
	const nbOutputFiles = getOutputFilesNumber();
	for (let i = 1; i <= nbOutputFiles; i++) {
		const text = fs.readFileSync(`./data/output${i}.txt`).toString('utf-8');
		outputsArray.push(text);
	}
	return outputsArray;
};

const schedulesToDetailedArray = (inputList) => {
	const schedules = inputList ? inputsToArray(inputList) : outputsToArray();

	return schedules.map(schedule => {
		const beginingAndEndingTime = schedule.slice(schedule.indexOf(' '));
		const day = schedule.slice(0, schedule.indexOf(' '));
		const dateStart = dateToLocalTime(new Date(`0${day}/10/2021 ${beginingAndEndingTime.slice(0, schedule.indexOf('-') - 1)}`));
		const dateEnd = dateToLocalTime(new Date(`0${day}/10/2021 ${beginingAndEndingTime.slice(schedule.indexOf('-'))}`));
		return {
			day,
			start: dateStart,
			end: dateEnd
		};
	}).sort((a, b) => a.day - b.day);
};

export const getFreeSchedule = (inputList, outputNumber) => {
	const outputs = schedulesToDetailedArray();
	const inputs = schedulesToDetailedArray(inputList);
	const isEveryoneFree = [];
	for (let i = 0; i <= inputs.length - 1; i++) {
		if (outputs[outputNumber].start > inputs[i].end || (outputs[outputNumber].start < inputs[i].end && outputs[outputNumber].start < inputs[i].start)
		) {
			isEveryoneFree.push(true);
		} else {
			isEveryoneFree.push(false);
		}
	}

	const scheduleDuration = getScheduleDuration(outputs[outputNumber]);
	const testStartDate = dateToLocalTime(new Date(`0${outputs[outputNumber].day}/10/2021 08:00`));
	const testEndDate = dateToLocalTime(new Date(`0${outputs[outputNumber].day}/10/2021 18:00`));

	if (isEveryoneFree.every(available => available) &&
		scheduleDuration === 59 &&
		outputs[outputNumber].start >= testStartDate &&
		outputs[outputNumber].end <= testEndDate) {
		return outputs[outputNumber];
	} else {
		outputNumber++;
		return getFreeSchedule(inputList, outputNumber);
	};
};

export const dateToLocalTime = (date) => {
	date.setTime(date.getTime() - new Date().getTimezoneOffset() * 60 * 1000);
	return date;
};

export const getScheduleDuration = (schedule) => Math.floor((schedule.end - schedule.start) / 60000);
