/* eslint-disable no-undef */
import { expect } from 'chai';
import { getFreeSchedule, getScheduleDuration, dateToLocalTime } from '../src/helpers.js';

describe('calendar tests', () => {
	it('Should throw if input file doesn\'t exist', () => {
		expect(() => {
			getFreeSchedule(10, 0);
		}).to.throw('this file doesn\'t exist');
	});
	it('Should not be before 08:00 am', () => {
		const freeSchedule = getFreeSchedule(2, 0);
		const testStartDate = dateToLocalTime(new Date(`0${freeSchedule.day}/10/2021 08:00`));

		// eslint-disable-next-line no-unused-expressions
		expect(freeSchedule.start >= testStartDate).to.be.true;
	});

	it('Should not be after 17:59 am', () => {
		const freeSchedule = getFreeSchedule(1, 0);
		const testEndDate = dateToLocalTime(new Date(`0${freeSchedule.day}/10/2021 18:00`));

		// eslint-disable-next-line no-unused-expressions
		expect(freeSchedule.end <= testEndDate).to.be.true;
	});

	it('Should last exactly 60 minutes', () => {
		const freeSchedule = getFreeSchedule(1, 0);

		const duration = getScheduleDuration(freeSchedule);
		expect(duration).to.equal(59);
	});

	it('Should always be at a time when everybody is free', () => {
		const freeSchedule = getFreeSchedule(2, 0);
		const freeSchedule2 = getFreeSchedule(1, 0);
		const freeSchedule3 = getFreeSchedule(4, 0);

		const scheduleComparison = {
			day: '2',
			start: dateToLocalTime(new Date(`0${freeSchedule.day}/10/2021 08:00`)),
			end: dateToLocalTime(new Date(`0${freeSchedule.day}/10/2021 08:59`))
		};

		const scheduleComparison2 = {
			day: '1',
			start: dateToLocalTime(new Date(`0${freeSchedule2.day}/10/2021 13:00`)),
			end: dateToLocalTime(new Date(`0${freeSchedule2.day}/10/2021 13:59`))
		};

		const scheduleComparison3 = {
			day: '2',
			start: dateToLocalTime(new Date(`0${freeSchedule3.day}/10/2021 12:29`)),
			end: dateToLocalTime(new Date(`0${freeSchedule3.day}/10/2021 13:28`))
		};

		expect(freeSchedule).to.deep.equal(scheduleComparison);
		expect(freeSchedule2).to.deep.equal(scheduleComparison2);
		expect(freeSchedule3).to.deep.equal(scheduleComparison3);
	});
});
